# Forget Me Not

A simple memory mapper to help debugging your structures you plan to use in dynamic libraries (.so/.dll) or through FFI.

**Not exactly a library you can link to your project yet.**


## Why the name?

Forget Me Not is the name of a flower (Myosotis).

I chose that name because this thing maps memory and forgetting is also related to memory, haha funny.


## But why?

Because if you use something like fix sized array of structs in a struct, you might have to write your own parser in ~~inferior~~ managed languages like C#.


## Aren't there better tools for this?

Most likely yes


## Building

You need cmake and fmt installed on your machine.

```bash
cmake -B build
#        ^~~ Directory

cmake --build build
#       ^     ^~~ Same directory
#       ^~~ Command
```

## Modes

For the `MemoryMapper` class you can set the mode either through the constructor or `setMode()` to the following enum values:
* `ABSOLUTE`: This mode will print the real memory addresses. This may result in the addresses changing between runs.
* `RELATIVE`: This mode will subtract the lowest memory address from every address, resulting in the memory addresses starting at 0. More consistent.

## Usage

Put your structure in `source/main.cpp` and then compile and run `build/ForgetMeNot`

### Standalone

Configure with the `-DBUILD_EXAMPLE=ON` flag and just insert your struct in:

#### Example

Example use

```cpp
struct Test {
    int foo;
    char bar;
    int baz[3];
};

int main() {
    Fmn::MemoryMap map(Fmn::MemoryMap::Mode::RELATIVE);

    Test test = {
        8,
        0x69,
        {1, 2, 3}
    };

    map.FMN_ADD(test); // Adds `test` as string along with the memory start and end addresses
    map.FMN_ADD(test.foo); // Overlapping members are not a problem
    map.FMN_ADD(test.foo); // Neither are duplicates, they are sorted and filtered out when needed
    map.FMN_ADD(test.bar);
    map.FMN_ADD(test.baz);

    for (size_t i = 0; i < 3; i++) {
        // Since `FMT_ADD()` takes the input as a string, you might want to replace indexes with their values
        map.FMN_ADD(test.baz[i]).FMN_INDEX(i);
    }

    std::cout << map << "\n";

    return 0;
}
```

Example output

![Example output](example.png)

### As a lib

Build this lib and link it to your executable, then you can use it as above.
