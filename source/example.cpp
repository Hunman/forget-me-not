#include <fmn/memory_map.hpp>
#include <iostream>

struct Test {
    int foo;
    char bar;
    int baz[3];
};

int main() {
    Fmn::MemoryMap map(Fmn::MemoryMap::Mode::RELATIVE);

    Test test = {
        8,
        0x69,
        {1, 2, 3}
    };

    map.FMN_ADD(test);
    map.FMN_ADD(test.foo);
    map.FMN_ADD(test.bar);
    map.FMN_ADD(test);
    map.FMN_ADD(test.baz);
    map.FMN_ADD(test.foo);

    for (size_t i = 0; i < 3; i++) {
        map.FMN_ADD(test);
        map.FMN_ADD(test.baz[i]).FMN_INDEX(i);
        map.FMN_ADD(test.baz);
    }

    std::cout << map << "\n";

    return 0;
}
