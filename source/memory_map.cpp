#include <fmn/memory_map.hpp>

#include <algorithm>
#include <utility>
#include <fmt/core.h>
#include <fmt/color.h>

namespace Fmn {
    MemoryMap::MemoryMap(): mode(Mode::ABSOLUTE), isSorted(true) {
        // ...
    }

    MemoryMap::MemoryMap(MemoryMap::Mode mode): mode(mode), isSorted(true) {
        // ...
    }

    void MemoryMap::sort() {
        if (isSorted) {
            return;
        }

        // Because I sort by start ASC, end DESC, if there are any overlaps,
        // the bigger one containing the smaller one is sooner
        std::sort(addresses.begin(), addresses.end(), [](const Address &a, const Address &b){
            if (a.start < b.start) {
                return true;
            }

            if (a.start == b.start) {
                return a.end > b.end;
            }

            return false;
        });

        auto last = std::unique(addresses.begin(), addresses.end(), [](const Address &a, const Address &b){
            return a.start == b.start
                && a.end == b.end;
        });
        addresses.erase(last, addresses.end());

        isSorted = true;
    }

    MemoryMap::Adder MemoryMap::add(const void *start, const void *end, const std::string &name) {
        return {{start, end, name}, [this](const Address &address){
            addresses.push_back(address);
            isSorted = false;
        }};
    }

    void MemoryMap::setMode(MemoryMap::Mode mode) {
        this->mode = mode;
    }

    void MemoryMap::add(const MemoryMap::Address &address) {
        addresses.push_back(address);
        isSorted = false;
    }

    MemoryMap::Adder::Adder(
        MemoryMap::Address &&address,
        const std::function<void(const MemoryMap::Address &address)> &adder
    ): address(std::move(address)), adder(adder) {
        // ...
    }

    MemoryMap::Adder::~Adder() {
        adder(address);
    }

    std::ostream &operator<<(std::ostream &out, MemoryMap &map) {
        if (map.addresses.empty()) {
            return out;
        }

        map.sort();

        auto charFor = [](const void *at, const MemoryMap::Address &address) {
            bool start = address.start == at;
            bool end = at + 1 == address.end;
            if (start && end) {
                return "◇";
            }

            if (start) {
                return "Ʌ";
            }

            if (end) {
                return "V";
            }

            return "|";
        };

        auto min = std::min_element(map.addresses.begin(), map.addresses.end(), [](const auto &a, const auto &b){
            return a.start < b.start;
        })->start;
        auto max = std::max_element(map.addresses.begin(), map.addresses.end(), [](const auto &a, const auto &b){
            return a.end < b.end;
        })->end;

        const void *offset = nullptr;
        if (map.mode == MemoryMap::Mode::RELATIVE) {
            offset = min;
        }

        size_t index = 0;
        std::vector<size_t> toPrint;

        // for (size_t i = 0; const auto &address: map.addresses) {
        //     fmt::print(fmt::fg(fmt::color::orange), "{:d} {:p} - {:p}: {:s}\n", i++, address.start, address.end, address.name);
        // }
        
        std::vector<fmt::terminal_color> colors;
        for (
            std::underlying_type<fmt::terminal_color>::type c = std::to_underlying(fmt::terminal_color::red);
            c <= std::to_underlying(fmt::terminal_color::white);
            c++
        ) {
            colors.push_back(static_cast<fmt::terminal_color>(c));
        }
        auto getColour = [&colors](size_t index){
            return fmt::fg(colors[index % colors.size()]);
        };

        for (auto p = min; p < max; p++) {
            // Remove the no longer needed ones
            while (!toPrint.empty() && map.addresses[toPrint.back()].end <= p) {
                // fmt::print(fmt::fg(fmt::color::red), "Removing: {:s}\n", toPrint.back()->name);
                toPrint.pop_back();
            }

            // Add the needed ones
            while (map.addresses[index].start == p) {
                // fmt::print(fmt::fg(fmt::color::green), "Adding: {:s}\n", map.addresses[index].name);
                toPrint.push_back(index++);
            }

            bool gap = index > 0
                && map.addresses[index - 1].end <= p
                && map.addresses[index].start > p;

            out << fmt::format(
                "{:#014X}: {:#04X}",
                reinterpret_cast<size_t>(p) - reinterpret_cast<size_t>(offset),
                static_cast<int>(
                    *reinterpret_cast<const unsigned char*>(p)
                )
            );

            for (const auto i: toPrint) {
                out << fmt::format(getColour(i), " {:s} {:s}", charFor(p, map.addresses[i]), map.addresses[i].name);
            }

            // for (const auto i: toPrint) {
            //     out << fmt::format(getColour(i), " {:s}", map.addresses[i].name);
            // }

            if (gap) {
                out << fmt::format(" X <GAP>");
            }

            out << "\n";
        }

        return out;
    }
}
