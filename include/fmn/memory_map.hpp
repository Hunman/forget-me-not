#pragma once

#include <functional>
#include <ostream>
#include <regex>
#include <string>
#include <vector>
#include <fmt/core.h>

#define FMN_ADD(EXP) add(&EXP, &EXP + 1, #EXP)
#define FMN_INDEX(INDEX) index(#INDEX, INDEX)

namespace Fmn {
    class MemoryMap {
    protected:
        struct Address {
            const void *start, *end;
            std::string name;
        };

    public:
        class Adder {
        public:
            Adder() = delete;
            Adder(Address &&address, const std::function<void(const MemoryMap::Address &address)> &adder);
            ~Adder();

            template <typename T>
            Adder &index(const char *name, const T &index) {
                std::regex regex(fmt::format("\\[{:s}\\]", name));

                address.name = std::regex_replace(address.name, regex, fmt::format("[{}]", index));

                return *this;
            }

        protected:
            Address address;
            std::function<void(const Address &address)> adder;
        };

        enum class Mode: uint8_t {
            ABSOLUTE = 0,
            RELATIVE
        };

        MemoryMap();
        MemoryMap(Mode mode);
        ~MemoryMap() = default;

        MemoryMap(const MemoryMap &) = delete;
        MemoryMap &operator=(const MemoryMap &) = delete;
        // Maybe later
        MemoryMap(MemoryMap &&) = delete;
        MemoryMap &operator=(MemoryMap &&) = delete;

        void setMode(Mode mode);

        Adder add(const void *start, const void *end, const std::string &name);

    protected:
        Mode mode;

        void add(const Address &adderss);

        std::vector<Address> addresses;

        bool isSorted;
        void sort();

        friend std::ostream &operator<<(std::ostream &out, MemoryMap &map);
    };
}
